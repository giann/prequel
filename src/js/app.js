$(document).ready(function () {

    Prequel = new Backbone.Marionette.Application();

    Prequel.sequellUrl = 'https://loom.shalott.org/api/sequell/';

    Prequel.scrollTo = function (target, offset, anim) {
        $('html, body').animate({
            scrollTop: target.offset().top - offset
        }, anim);
    };

    Prequel.navigate = function () {
        return Backbone.history.navigate.apply(Backbone.history, arguments);
    };

    Prequel.addRegions({
        searchRegion: '#search',
        resultRegion: '#result'
    });

    Prequel.on('initialize:after', function () {
        if (Backbone && Backbone.history) {
            Backbone.history.start();
        }
    });

    Prequel.module('Crawl', {

        define: function (module, application) {
            // init controllers
            this.controllers = {};
            
            // init routers
            this.routers = {};
            this.addInitializer(function () {
                this.routers.search = new SearchRouter();
                this.routers.game = new GameRouter();
                this.routers.milestone = new MilestoneRouter();
            });
            
            // reset routers
            this.addFinalizer(function () {
                BRS.each(module.routers, function (router) {
                    router.unbindAll();
                });
                module.routers = {};
            });
            
            // reset controllers
            this.addFinalizer(function () {
                BRS.each(module.controllers, function (controller) {
                    controller.unbindAll();
                });
                module.controllers = {};
            });
        }
    });

    Prequel.addInitializer(function () {
        this.Crawl.start();
    });

    Prequel.start();

    Prequel.vent.trigger('initial');

});