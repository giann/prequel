GameController = Backbone.Marionette.Controller.extend({

    initialize: function () {
        Prequel.vent.on('search', this.showGameList.bind(this));
    },

    showGameList: function (query) {
        var gamesCollection = new Games(),
            gamesView = new GamesView(),
            sequell;

        sequell = new Promise(function (resolve, reject) {

            $.getJSON(Prequel.sequellUrl + 'game?q=' + query, function (data) {
                resolve(data);
            });

        });

        sequell.then(function (data) {
            _.each(data.records, function (record) {
                gamesCollection.add(record);
            });
        });

        gamesView.collection = gamesCollection;
    
        Prequel.resultRegion.reset();
        Prequel.resultRegion.show(gamesView);
    }

});
