SearchView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',

    className: 'search',
    
    template: '#search-template',

    ui: {
        'search': '.search-input'
    },

    events: {
        'keyup @ui.search': 'showSearch',
    },

    onDomRefresh: function () {
        
    },

    showSearch: function (event) {

        if (event.keyCode == 13) {
            Prequel.vent.trigger('search', this.ui.search[0].value);
        }

    }

});