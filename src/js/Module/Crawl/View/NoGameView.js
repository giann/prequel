/*global Backbone:true */

NoGameView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',

    className: 'game empty',
    
    template: '#no-game-template',

    onDomRefresh: function () {
    }
    
});