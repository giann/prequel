GameView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',

    className: 'game row',
    
    template: '#game-template',

    events: {
    },

    onDomRefresh: function () {
    },

    initialize: function (options) {
    }

});