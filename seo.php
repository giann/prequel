<?php
    $fragment = $_GET['_escaped_fragment_'];
    $matches = array();
    $post_id = null;
    $post = null;
    $last_posts = null;
    
    $last_posts = json_decode(file_get_contents('http://papyrus87.fr/wordpress/?json=get_posts&count=10'))->posts;

    if (preg_match("/post\/(\\d+)/", $fragment, $matches)) {
        $post_id = $matches[1];
    }

    if ($post_id) {
        $post = json_decode(file_get_contents('http://papyrus87.fr/wordpress/?json=get_post&post_id=' . $post_id))->post;
    } else {
        $post = $last_posts[0];
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1"/>
    
    <title>Otohptra</title>

    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <link rel="shortcut icon" href="img/favicon.png" type="image/png"/>

</head>

<body>

    <nav>
        <? foreach ($last_posts as $p) { ?>
            <a href="/#post/<?= $p->id ?>"><?= $p->title ?></a>
        <? } ?>
    </nav>

    <? if ($post) { ?>
        <article>
            <h1><?= $post->title ?></h1>
            <?= $post->content ?>
        </article>
    <? } ?>

</body>

</html>