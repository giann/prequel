module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
                separator: ';\n',
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                src: ['src/js/mo.js'],
                dest: 'js/<%= pkg.name %>.js'
            },
            libs: {
                src: [
                    "src/lib/jquery-1.9.0.min.js"
                ],
                dest: 'lib/<%= pkg.name %>-libs.js'   
            },
            css: {
                src: [
                    'src/css/font.css',
                    'src/css/icomoon.css',
                    'src/css/elements.less.css',
                    'src/css/giann.less.css',
                    'src/css/prequel.less.css'
                ],
                dest: 'css/<%= pkg.name %>.less.css'
            }
        },

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                src: 'js/<%= pkg.name %>.js',
                dest: 'js/<%= pkg.name %>.min.js'
            },
            libs: {
                src: 'lib/<%= pkg.name %>-libs.js',
                dest: 'lib/<%= pkg.name %>-libs.min.js'
            }
        },

        compress: {
            main: {
                options: {
                    mode: 'gzip'
                },

                files: [
                    {
                        expand: true,
                        src: 'lib/<%= pkg.name %>-libs.min.js', 
                        dest: '',
                        ext: '.gz.min.js'
                    },
                    {
                        expand: true,
                        src: 'js/<%= pkg.name %>.min.js', 
                        dest: '',
                        ext: '.gz.min.js'
                    }
                ]
            }
        },

        less: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                src: 'css/<%= pkg.name %>.less.css',
                dest: 'css/<%= pkg.name %>.css'
            }
        },

        cssmin: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                src: 'css/<%= pkg.name %>.css',
                dest: 'css/<%= pkg.name %>.min.css'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', ['concat', 'less', 'cssmin', 'uglify', 'compress']);

};