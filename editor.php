<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1"/>
    
    <title>Papyrus 87</title>

    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="fragment" content="!">

    <link rel="shortcut icon" href="img/favicon.png" type="image/png"/>

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="src/css/font.css">
    <link rel="stylesheet" type="text/less" href="src/css/papyrus87.less.css">
    <link rel="stylesheet" type="text/css" href="src/css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="src/css/pen.css">

    <script id="full-post-template" type="text/template">
        <div class="full-post">
            <div class="full-post-header">
                <h1 class="full-post-title">
                    <i class="focus icon-menu2"></i>
                    <%= title %>
                </h1>
            </div>
            <img src="img/line.png" class="line"/>
            <div class="full-post-info">
                <div class="full-post-date">
                    <i class="icon-clock"></i><%= moment(date).format('dddd Do MMMM YYYY, HH:mm:ss') %>
                </div>  
                <div class="full-post-tags">
                    <i class="icon-tag"></i>
                    <input name="search-tags" type="text" size="30"/>
                    <% if (tags.length > 0) { 
                        var tags = tags.split(', '); %>
                        <% _.each(tags, function (tag) {
                            if (tag.length > 1) { %>
                            <span class="full-post-tag">
                                <%= tag %>
                                <i class="icon-close"></i>
                            </span>
                            <% } %>
                        <% }) %>
                    <% } %>
                </div>
                <div class="full-post-categories">
                    <% if (categories.length > 0) { %>
                        <% if (categories.length > 1 || categories[0].slug !== 'non-classe') { %>
                            <i class="icon-books"></i>
                            <% _.each(categories, function (category) { %>
                                <span class="full-post-category">
                                    <%= category.title %>
                                </span>
                            <% }) %>
                        <% } %>
                    <% } %>
                </div>
                <div class="cb"></div>
            </div>
            <div class="full-post-text hide"><%= content %></div>

            <div class="full-post-text-area">
                <textarea><%= $('<div>' + content + '</div>').find('.full-post-markdown').html() %></textarea>
            </div>

            <div class="full-post-footnotes">
                <% if (footnotes.length > 0) { %>
                    <h5>Notes de bas de page:</h5>
                    <% for (footnote in footnotes) { %>
                        <div class="full-post-footnote" id="footnote-<%= (parseInt(footnote)+1) %>">
                            <span class="footnote"><%= (parseInt(footnote)+1) %></span>
                            <%= footnotes[footnote] %>
                            <i class="icon-close <%= (parseInt(footnote)+1) %>"></i>
                        </div>
                    <% } %>
                <% } %>
            </div>

            <% if (photos !== null) { %>
                <div class="full-post-set">
                    <div class="loader">
                        <img src="img/spinner.gif"></img>
                    </div>
                    <div class="set">
                        <% _.each(photos, function (photo) { %>
                            <img src="<%= Intaglio.buildPhotoURL(photo, 'z') %>"/>
                        <% }); %>
                    </div>
                </div>
            <% } %>

            <div class="full-post-images">
                <h5>Images:</h5>
                <% if (attachments.length > 0) { %>
                    <% for (a in attachments) {  %>
                        <% var attachment = attachments[a],
                               url = attachment.images && attachment.images.medium ? attachment.images.medium.url : attachment.url; %>
                            <div class="full-post-image">
                                <img src="<%= url %>" alt="<%= attachment.caption %>"/>
                                <div class="full-post-image-code">
                                    <%= attachment.slug %><!--<i class="icon-close <%= attachment.slug %>"></i>-->
                                </div>
                            </div>
                    <% } %>
                <% } %>

                <div class="full-post-image upload">
                    <form name="imageForm">
                        <input type='file' name='userFile'>
                    </form>
                </div>
            </div>

            <% if (places.length > 0) { %>
                <div class="full-post-locations-ref">
                    <h5>Références géographiques:</h5>
                    <% for (l in places) {  %>
                        <% var loc = places[l],
                            code = loc.code,
                            lat = loc.lat,
                            lon = loc.lon; %>

                        <div class="full-post-location-ref <%= code %>">
                            <i class="icon-location"></i> <%= code %> <i class="icon-close <%= code %>"></i>
                        </div>
                    <% } %>
                </div>
            <% } %>

            <div class="full-post-locations<%= places.length > 0 ? ' show' : ''%>">
                <div class="pac">
                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                    <span class="pac-button button"><i class="icon-plus"></i></span>
                </div>
                <div class="full-post-location">
                    
                </div>
            </div>

            <i class="icon-screen"></i>
        </div>
    </script>

    <script id="thumb-post-template" type="text/template">
        <div class="post" id="#post-<%= id %>">
            <h4 class="post-title"><%= title %></h4>
            <div class="post-description"><%= excerpt %></div>
            <div class="post-timestamp"><%= moment(date, 'YYYY-MM-DD HH:mm:SS').fromNow() %></div>
            <div class="post-status"><%= status === 'publish' ? 'publié' : 'brouillon' %></div>
            <i class="icon-close"></i>
        </div>
    </script>

    <script id="posts-template" type="text/template">
        <div class="blog-info">
            <img src="img/logo-small.png" alt="Papyrus87">
            <h1 class="blog-info-title">Papyrus<span class="exp">87</span></h1>
            <div class="blog-info-presentation">
                Manuscrits. &Eacute;critures. R&eacute;critures. Lectures.
            </div>
        </div>

        <div class="post-list">
        </div>

        <div class="credentials">
            &copy; 2014 - Tout droits r&eacute;serv&eacute;s - <a target="_blank" title="Giann Web cr&eacute;ateur de sites web" href="http://giann.fr"><img src="img/giann.png" alt="Giann Web cr&eacute;ateur de sites web"/>Giann</a>
        </div>

        <div class="post-list-nav <%= pages <= 1 ? 'no-page' : '' %>">
            <span class="nav-previous" page="<%= parseInt(page) > 1 ? parseInt(page)-1 : 1 %>"><i class="icon-arrow-left2"></i></span><span class="nav-next" page="<%= parseInt(page) < parseInt(pages) ? parseInt(page)+1 : parseInt(page) %>"><i class="icon-arrow-right"></i></span>
        </div>
    </script>

    <script id="photo-overlay-template" type="text/template">
        <img src="<%= url %>" alt=""/>
    </script>

    <script id="no-comment-template" type="text/template">
        <div class="comment-content">
            Pas de commentaire
        </div>
    </script>

    <script id="comment-template" type="text/template">
        <div class="comment-content">
            <%= content %>
        </div>
        <div class="comment-info">
            <div class="comment-date">
                <i class="icon-clock"></i>
                <%= moment(date, 'YYYY-MM-DD HH:mm:SS').fromNow() %>
            </div>
            <div class="comment-author">
                par <b><%= name %></b>
            </div>
            <div class="cb"></div>
        </div>
    </script>

    <script id="comments-template" type="text/template">
        <div class="comment-response">
        </div>
        <div class="button grey comment show">
            Poster un commentaire
        </div>
        <div class="comment-entry">

            <div class="comment-form">
                <label for="author">Nom <span class="required">*</span></label>
                <input name="author" type="text" value="" size="30" aria-required="true"/>

                <label for="email">Email <span class="required">*</span></label>
                <input name="email" type="email" value="" size="30" aria-required="true"/>

                <label for="comment">Commentaire <span class="required">*</span></label>
                <textarea name="comment" rows="8" aria-required="true"></textarea>

                <div class="button grey comment-submit">
                    Poster
                </div>
            </div>
        </div>

        <div class="comment-list">
        </div>
    </script>

    <script id="edit-toolbar-template" type="text/template">

        <div class="action create">
            <i class="icon-plus"></i>
            <span class="action-tooltip">
                Nouveau
            </span>
        </div>

        <div class="action delete">
            <i class="icon-minus"></i>
            <span class="action-tooltip">
                Supprimer
            </span>
        </div>

        <div class="action help">
            <i class="icon-question"></i>
            <span class="action-tooltip">
                Aide
            </span>
        </div>

        <div class="action save">
            <i class="icon-disk"></i>
            <span class="action-tooltip">
                Sauvegarder
            </span>
        </div>

        <div class="action publish">
            <i class="icon-newspaper"></i>
            <span class="action-tooltip">
                Publier
            </span>
        </div>

        <div class="action preview">
            <i class="icon-eye"></i>
            <span class="action-tooltip">
                Prévisualiser
            </span>
        </div>

        <div class="action code">
            <i class="icon-code"></i>
            <span class="action-tooltip">
                Code/Présentation
            </span>
        </div>

        <div class="action image">
            <i class="icon-image"></i>
            <span class="action-tooltip">
                Image
            </span>
        </div>

        <div class="action location">
            <i class="icon-location"></i>
            <span class="action-tooltip">
                Carte
            </span>
        </div>

    </script>

    <script id="result-template" type="text/template">
        <h3 class="result-title">
            <a href="#!post/<%= id %>"><%= title %></a>
        </h3>
        <div class="result-excerpt">
            <%= excerpt %>
        </div>
        <div class="result-timestamp">
            <%= moment(date, 'YYYY-MM-DD HH:mm:SS').fromNow() %>
        </div>
    </script>

    <script id="no-result-template" type="text/template">
        <h3 class="result-title">
            Aucun article correspondant.
        </h3>
    </script>

    <script id="results-template" type="text/template">
        <div class="result-list">
        </div>
    </script>

    <script id="help-template" type="text/template">
        <div class="blog-info">
            <img src="img/logo-small.png" alt="Papyrus87">
            <h1 class="blog-info-title">Papyrus<span class="exp">87</span></h1>
            <div class="blog-info-presentation">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris.
            </div>
        </div>

        <div class="help-list">
            <div class="help">
                <div class="command code">
                    {location{code}{mot-clé}}
                </div>
                <div class="definition">
                    Insère dans le texte un <span class="code">mot-clé</span> désignant la carte correspondant au <span class="code">code</span>.
                </div>
            </div>

            <div class="help">
                <div class="command code">
                    {map{code}}
                </div>
                <div class="definition">
                    Insère dans le texte la carte correspondant au <span class="code">code</span>.
                </div>
            </div>

            <div class="help">
                <div class="command code">
                    {foot{numéro}{note}}
                </div>
                <div class="definition">
                    Insère une note de bas de page avec le <span class="code">numéro</span> et le contenu <span class="code">note</span> spécifiés.
                </div>
            </div>

            <div class="help">
                <div class="command code">
                    {img{code}{qualité}}
                </div>
                <div class="definition">
                    Insère l'image correspondant au <span class="code">code</span> avec la <span class="code">qualité</span> spécifiée (<span class="code">thumbnail</span>, <span class="code">medium</span> ou <span class="code">full</span>).
                </div>
            </div>
        </div>

        <div class="credentials">
            &copy; 2014 - Tout droits r&eacute;serv&eacute;s - <a target="_blank" title="Giann Web cr&eacute;ateur de sites web" href="http://giann.fr"><img src="img/giann.png" alt="Giann Web cr&eacute;ateur de sites web"/>Giann</a>
        </div>
    </script>

</head>

<body>

    <div class="intaglio editor">

        <section id="left-region">

        </section>

        <div class="content">
            <section id="main-region"></section>
        </div>

        <nav id="nav-region">
            
        </nav>

        <div id="image-overlay-region">
            
        </div>
    </div>
    
    <script type="text/javascript" src="src/lib/less-1.3.3.min.js"></script>
    <script type="text/javascript" src="src/lib/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="src/lib/jquery.collagePlus.js"></script>
    <script type="text/javascript" src="src/lib/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="src/lib/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="src/lib/modernizr.js"></script>
    <script type="text/javascript" src="src/lib/lodash.js"></script>
    <script type="text/javascript" src="src/lib/moment.js"></script>
    <script type="text/javascript" src="src/lib/md5.min.js"></script>
    <script type="text/javascript" src="src/lib/typeahead.bundle.js"></script>
    <script type="text/javascript" src="src/lib/backbone.js"></script>
    <script type="text/javascript" src="src/lib/backbone.wreqr.js"></script>
    <script type="text/javascript" src="src/lib/backbone.babysitter.js"></script>
    <script type="text/javascript" src="src/lib/backbone.marionette.last.js"></script>

    <script type="text/javascript" src="src/lib/pen.js"></script>
    <script type="text/javascript" src="src/lib/markdown.js"></script>
    <script type="text/javascript" src="src/lib/markdown-parser.js"></script>

    <script type="text/javascript" src="src/js/Module/Post/Model/Post.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Model/Photo.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Model/Comment.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Collection/Posts.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Collection/Comments.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Controller/PostController.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Controller/EditorController.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Controller/CommentController.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Router/PostRouter.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Router/EditorRouter.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/Router/CommentRouter.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/EditToolbarLayout.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/FullPostView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/EditPostView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/CommentView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/NoCommentView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/EditThumbPostView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/ThumbPostView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/PostsView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/NoSearchResultView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/SearchResultView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/SearchResultsView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/CommentsView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/HelpView.js"></script>
    <script type="text/javascript" src="src/js/Module/Post/View/PhotoOverlayView.js"></script>

    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places&key=AIzaSyDvTCiR5cRLvGzafk8v-fx9rh7qPKRSfaY"></script>

    <script type="text/javascript" src="src/js/editor.js"></script>

</body>

</html>
